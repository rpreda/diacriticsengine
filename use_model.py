from keras.models import load_model
from keras.models import Model, Sequential
from keras.layers import Input, LSTM, Dropout, TimeDistributed, Dense, Bidirectional, Embedding, GRU
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.models import load_model
from keras import backend as K
from utils import predict, remove_dia


model = load_model('saved_model.h5')
with open('ShortTest', 'r') as f:
    test_text = ''.join(f.readlines()).lower()

result_text = predict(model, remove_dia(test_text), True)
# correct = 0
# wrong = 0
# for idx, ch in enumerate(test_text):
#     if test_text[idx] in ['â', 'î', 'ț', 'ș', 'ă']:
#         if result_text[idx] == test_text[idx]:
#             correct += 1
#         else:
#             wrong += 1
# print('Correct: ' + str(correct), 'Wrong: ' + str(wrong))

