from text_utils import *
from keras.models import Model
from keras.layers import Input, LSTM, Dropout, TimeDistributed, Dense, Bidirectional, Embedding
from sklearn.model_selection import train_test_split
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.models import load_model


def init_neural_newtowrk():
    inputs = Input(shape=(30, 1))
    x = Bidirectional(LSTM(128, return_sequences=True))(inputs)
    x = Dropout(0.25)(x)
    x = TimeDistributed(Dense(OUTPUT_SIZE, activation='softmax'))(x)

    model = Model(inputs=inputs, outputs=x)

    model.compile('adam', 'categorical_crossentropy', metrics=['acc'])

    return model


model = init_neural_newtowrk()


def predict(text):
    X = break_into_blocks(text_to_sequence(text))
    X = np.reshape(X, X.shape + (1,))
    pred = model.predict(X)
    pred = pred.reshape(-1, pred.shape[-1])

    out = []
    labels = [np.argmax(amax) for amax in pred[:len(text)]]

    for i, label in enumerate(labels):
        if label == 3: out.append(text[i])
        if label == 2: out.append('â')
        if label == 1: out.append('î')
        if label == 0: out.append('#')

    print(text)
    print(''.join(out))


with open('TrainText.txt', 'r') as f:
    dataset = ' '.join(f.readlines())

X = break_into_blocks(text_to_sequence(remove_dia(dataset)))
X = np.reshape(X, X.shape + (1,))

Y = to_target_array(dataset)

#Split data into test and train
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, random_state=42)



def test(epoch, logs):
    text = "Merg acasa insotit de niste oameni si doi caini care alearga repede"
    predict(text)

pred = model.predict(X)

model.fit(X_train, y_train, validation_data=(X_test, y_test),  epochs=100, batch_size=32, callbacks=[
    LambdaCallback(test), ModelCheckpoint('save', monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
])


#Testing
text = "Merg acasa insotit de niste oameni si doi caini care alearga repede"
X = break_into_blocks(text_to_sequence(text))
X = np.reshape(X, X.shape + (1,))
pred = model.predict(X)
pred = pred.reshape(-1, pred.shape[-1])

out = []
labels = [np.argmax(amax) for amax in pred[:len(text)]]

print(text)
print(labels)
model.save("saved_model.h5")

#
# import tensorflow as tf
# import tensorflow.keras as keras
# import matplotlib.pyplot as plt

#Intial test with keras
#
# mnist = tf.keras.datasets.mnist
#
#
# (x_train, y_train),(x_test, y_test) = mnist.load_data()
# x_train = tf.keras.utils.normalize(x_train, axis=1).reshape(x_train.shape[0], -1)
# x_test = tf.keras.utils.normalize(x_test, axis=1).reshape(x_test.shape[0], -1)
#
#
# model = tf.keras.models.Sequential()
# model.add(tf.keras.layers.Flatten())
# model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
# model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
# model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))
# model.compile(optimizer='adam',
#               loss='sparse_categorical_crossentropy',
#               metrics=['accuracy'])
# model.fit(x_train, y_train, epochs=3)
# val_loss, val_acc = model.evaluate(x_test, y_test)
# print(val_loss, val_acc)
