import numpy as np
from keras.utils import to_categorical

OUTPUT_SIZE = 6
LEN_OF_SAMPLE = 20
LSTM_SIZE = 128
conv_tab = {
    "ă": [1],
    "ș": [2],
    "ț": [3],
    "î": [4],
    "â": [5],
}
conv_tab_reverse = {
    1: "ă",
    2: "ș",
    3: "ț",
    4: "î",
    5: "â",
}


def text_to_sequence(text):
    return np.array([ord(c) for c in text])


def break_into_blocks(data, block_length=LEN_OF_SAMPLE):
    data = np.array(data, dtype=int)
    padding = block_length - len(data) % block_length
    data = np.append(data, np.zeros(dtype=int, shape=(1, padding)))
    data = np.reshape(data, newshape=(int(len(data) / block_length), -1))
    return data


def remove_dia(text):
    trans_tab = {
        "î": "i",
        "ă": "a",
        "ț": "t",
        "â": "a",
        "ș": "s",
        "Î": "I",
        "Ă": "A",
        "Ț": "T",
        "Â": "A",
        "Ș": "S",
    }
    for char in trans_tab.keys():
        text = text.replace(char, trans_tab[char])
    return text


def to_target_array(text):
    ret_val = []
    for char in text.lower():
        if char in ["ă", "ș", "ț", "î", "â"]:
            ret_val.append(conv_tab[char])
        else:
            ret_val.append([0])
    ret_val = break_into_blocks(ret_val)
    ret_val = to_categorical(ret_val, OUTPUT_SIZE)
    return ret_val


def predict(model, text, out_txt=False):
    X = break_into_blocks(text_to_sequence(text))
    X = np.reshape(X, X.shape + (1,))
    Y_pred = model.predict(X)
    Y_pred = Y_pred.reshape(-1, Y_pred.shape[-1])
    out = []
    labels = [np.argmax(amax) for amax in Y_pred[:len(text)]]
    for i, label in enumerate(labels):
        if label == 0:
            out.append(text[i])
        else:
            out.append(conv_tab_reverse[label])
    if (out_txt):
        print(''.join(out))
    return ''.join(out)
