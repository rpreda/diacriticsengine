import numpy as np
from keras.utils import to_categorical

OUTPUT_SIZE = 4


def text_to_sequence(text):
    return np.array([ord(c) for c in list(text)])


def break_into_blocks(arr, n=30):
    arr = np.array(arr, dtype=int)

    if len(arr) % n:
        pad_size = n - (len(arr) % n)
        pad = np.zeros((1, pad_size), dtype=int)
        arr = np.append(arr, pad)

    arr = np.reshape(arr, (int(len(arr) / n), -1))

    return arr


def fix_dia(text):
    trans_tab = {
        "ş": "ş",
        "ţ": "ţ",
    }

    for char in trans_tab.keys():
        text = text.replace(char, trans_tab[char])

    return text


def remove_dia(text):
    text = fix_dia(text)

    trans_tab = {
        "î": "i",
        "ă": "a",
        "ț": "t",
        "â": "a",
        "ș": "s",
        "Î": "I",
        "Ă": "A",
        "Ț": "T",
        "Â": "A",
        "Ș": "S",
    }

    for char in trans_tab.keys():
        text = text.replace(char, trans_tab[char])
    return text


def to_target_array(text):
    text = fix_dia(text)

    returnable = []
    for char in text.lower():
        if char in ["ă", "ș", "ț"]:
            returnable.append([0])
        elif char in ["î"]:
            returnable.append([1])
        elif char in ["â"]:
            returnable.append([2])
        else:
            returnable.append([3])

    returnable = break_into_blocks(returnable)
    #Creates the binary matrices required for the results
    returnable = to_categorical(returnable, OUTPUT_SIZE)

    return returnable


