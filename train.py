from keras.models import Model, Sequential
from keras.layers import Input, LSTM, Dropout, TimeDistributed, Dense, Bidirectional, Embedding, GRU
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.models import load_model
from keras import backend as K
from utils import *

def neural_model_v1():
    model = Sequential()
    model.add(LSTM(LSTM_SIZE, return_sequences=True, input_shape=(LEN_OF_SAMPLE, 1)))
    model.add(Dense(OUTPUT_SIZE, activation='softmax'))
    model.compile('adam', 'categorical_crossentropy', metrics=['accuracy'])
    return model

def neural_model_v2():
    model = Sequential()
    model.add(Bidirectional(LSTM(128, return_sequences=True, input_shape=(LEN_OF_SAMPLE, 1))))
    model.add(Dense(OUTPUT_SIZE, activation='softmax'))
    model.compile('adam', 'categorical_crossentropy', metrics=['accuracy'])
    return model

def neural_model_v3():
    model = Sequential()
    model.add(Bidirectional(LSTM(128, dropout=0.2, return_sequences=True, input_shape=(LEN_OF_SAMPLE, 1))))
    model.add(TimeDistributed(Dense(OUTPUT_SIZE, activation='softmax')))
    model.compile('adam', 'categorical_crossentropy', metrics=['accuracy'])
    return model

model = neural_model_v2()

with open('TrainTestData', 'r') as f:
    dataset = ' '.join(f.readlines())

X = break_into_blocks(text_to_sequence(remove_dia(dataset)))
X = np.reshape(X, X.shape + (1,))
Y = to_target_array(dataset)
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=12341)
def test(epoch, logs):
    predict(model, 'Apoi am inchis, cu senzatia ca facusem o treaba buna. Plantasem prima samanta.', True)
model.fit(X_train, y_train, validation_data=(X_test, y_test),  epochs=1, batch_size=32, callbacks=[
    LambdaCallback(test), ModelCheckpoint('model_temp', monitor='val_loss', verbose=1, save_best_only=False, save_weights_only=False, mode='auto', period=1)
    ])
test(0, 0)
model.save("saved_model_bidirectional.h5")
